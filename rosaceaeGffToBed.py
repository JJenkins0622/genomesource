#!/usr/bin/env python

"Read a Rosaceae.org GFF file and write out a BED version"

import os,sys,re,optparse
import Mapping.Parser.Bed as Bed
import Mapping.Parser.PhytozomeGff as p
import Mapping.FeatureModel

def readGffFile(fname=None):
    models = p.gff2feats(fname)
    for m in models:
        cdss = m.getFeats(feat_type='CDS')
        for cds in cdss:
            exon = Mapping.FeatureModel.DNASeqFeature(feat_type="exon",
                                                      strand=cds.getStrand(),
                                                      start=cds.getStart(),
                                                      length=cds.getLength(),
                                                      seqname=cds.getSeqname())
            m.addFeat(exon)
    
    return models

def writeBedFile(fname=None,feats=None):
    Bed.feats2bed(feats,fname=fname)

def convert(bed_file=None,
            gff_file=None):
    feats = readGffFile(fname=gff_file)
    writeBedFile(fname=bed_file,feats=feats)

def main(bed_file=None,gff_file=None):
    convert(bed_file=bed_file,
            gff_file=gff_file)

if __name__ == '__main__':
    usage = "%prog [options]"
    parser = optparse.OptionParser(usage)
    parser.add_option("-g","--gff_file",help="GFF file to convert",dest="gff_file"),
    parser.add_option("-b","--bed_file",help="BED format file to write",
                      dest="bed_file",default=None)
    (options,args)=parser.parse_args()
    if not options.bed_file or not options.gff_file:
        parser.error("Bed or GFF files not specified.")
    main(gff_file=options.gff_file,
         bed_file=options.bed_file)
