#!/usr/bin/env python

"""
Convert BED to GTF.  
"""

import fileinput, sys, optparse, re
import Mapping.Parser.Bed as b

def main(symbol=False,args=None):
    feats=[]
    for line in fileinput.input(args):
        feat=bedline2feat(line)
        feats.append(feat)
    return feats

if __name__ == '__main__':
    usage = '%prog [options] [FILE...]'
    parser=optparse.OptionParser(usage)
    parser.add_option('-s','--symbol',help="use symbol (field 13) to indicate gene",
                      dest='symbol',default=False,action="store_true")
    (options,args)=parser.parse_args()
    main(include_symbol=options.include_symbol,args=args)
