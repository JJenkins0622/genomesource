"""Functions for performing IGB-related tasks.

Note: This module contains the functions that can make target features, e.g., a section of a consensus feature that contains only the region covered by probes. So far as I know, neither Affymetrix nor UCSC provides these."""

import string,sys,os,re,dbm

import Utils.General as su
import Utils.Affy as au
import Mapping.FeatureFactory as ff
import Mapping.Feature

sep1 = '\t'
sep2 = ','
    
def feats2gn(txs,f='feats.gn'):
    """
    Function: using the given protein-coding feature objects create an ASCII 'gn' format file
    Returns : nothing
    Args    : txs - a dictionary of feature objects, keys are the display_id
              f - the name of the 'gn' format file to write

details on the gn format:
    
        name UTF8
        chrom UTF8
        strand UTF8
        txStart int
        txEnd int
        cdsStart int
        cdsEnd int
        exoncount int
        exonStarts int[exoncount]
        exonEnds int[exoncount]
  
"""
    w = open(f,'w')
    for tx in txs.values():
        line = feat2gn_line(tx)
        w.write(line)
    w.close()

def feat2gn_line(tx):
    name = tx.display_id()
    strand = tx.strand()
    try:
        ORF = tx.feats(type='ORF')[0]
        cds_start = ORF.start()
        cds_end = ORF.start() + ORF.length()
    except TypeError:
        try: 
            CDSs = tx.sorted_feats(type='CDS')
            if strand == 1:
                cds_start = CDSs[0].start()
                cds_end = CDSs[-1].start()+CDSs[-1].length()
            else:
                cds_start = CDSs[-1].start()+CDSs[-1].length()
                cds_end = CDSs[0].start()
        except TypeError:
            sys.stderr.write("Warning: can't get ORF for transcript: " + name + "\nIgnoring it for now.\n")
            return None
    tmin = tx.start()
    tmax = tx.start() + tx.length()
    chrom = tx.seqname()
    if strand == 1:
        strand = '+'
    else:
        strand = '-'
    exons = tx.feats(type='exon')
    ecount = len(exons)
    emins = map(lambda x: repr(x.start()),exons)
    exon_starts = string.join(emins,',')
    emaxs = map(lambda x: repr(x.start() + x.length()),exons)
    exon_ends = string.join(emaxs,',')
    vals = [name,chrom,strand,repr(tmin),repr(tmax),repr(cds_start),
            repr(cds_end),repr(ecount),exon_starts,exon_ends]
    line = string.join(vals,'\t')
    return line+'\n'
    
def write_bgn(text_file,
              path=None):
    """
    Function: reads the ASCII format file (output from feats2gn)
              and writes it out in IGB-compatible bgn format
              the file that is written has the same name as the input
              text_file but with a 'bgn' extension
    Returns : nothing
    Args    : text_file - string, name of a text 'gn' format file to
                          convert
              path - directory containging BgnConvert.class

    If not given, will use $HOME/src/at_annots/src
    """
    toks = text_file.split('.')
    if toks[-1] == 'gn':
        bn_file = string.join(toks[0:-1],'.')+'.bgn'
    else:
        bn_file = text_file+'.bgn'
    sys.stderr.write("About to write: " + bn_file + '\n')
    os.system('java -classpath $HOME/src/at_annots/src BgnConverter ' + \
              text_file + ' ' + bn_file)
    # BgnConverter text_file bin_file

def get_probe_coords(seq,probe):
    """
    Function: Find the given probe's location in the given sequences
    Returns : start coordinate (interbase) 
    Args    : seq - the sequence to search for exact match by the probe
              probe - the probe sequence to search
    """
    reg = re.compile(probe,re.I)
    iter = reg.finditer(seq)
    coords = []
    while 1:
        try:
            match = iter.next()
        except StopIteration:
            break
        coords.append(match.start())
    return coords


def list_pss(feats,fn='filtered_430_probesets.txt'):
    """
    Function: Write out a list of the given feats' display id's
              Delegates to feat_utils.write_dids
    Returns :
    Args    : fn - name of file to write (write to stderr if None)
    """
    ff.write_dids(feats,fn=fn)

def filter_cons(feats,nprobes=None):
    okays = []
    for feat in feats:
        pc = feat.get_val('probe_coords')
        if len(pc.keys())==nprobes:
            multi_probes = filter(lambda x:len(pc[x])>1,pc.keys())
            if len(multi_probes)==0:
                okays.append(feat)
    return okays
                                  
def map_coord(feat=None,c=None,type='exon'):
    subfeats = feat.feats(type=type)
    subfeats.sort(lambda x,y:x.start()-y.start())
    L = 0
    for subfeat in subfeats:
        mstart = L
        mend = mstart + subfeat.length()
        if c >= mstart and c <= mend:
            gc = c - mstart + subfeat.start()
            return gc
        L = L + subfeat.length()
    return None

def get_min_max(feat,probe_length=25):
    mx = None
    mn = None
    pcs = feat.get_val('probe_coords')
    for p in pcs.keys():
        lst = pcs[p]
        candi_mx = max(lst)
        candi_mn = min(lst)
        if mx==None:
            mx = candi_mx
            mn = candi_mn
        else:
            if candi_mx > mx:
                mx = candi_mx
            if candi_mn < mn:
                mn = candi_mn
    return (mn,mx+probe_length)

def trim_cons_feat(feat,probe_length=25):
    """
    Function: Create a new consensus sequence featur that includes just the
              region defined by the probe coordinates
    Returns : 
    Args    : feat - a CompoundDNASeqFeature object, with probe coordinates
                     added (note that the probe coordinates refer to the
                     spliced mRNA sequence, not the genomic.) 
    """
    (mn,mx)=get_min_max(feat,probe_length=probe_length)
    newpcs = {}
    pcs = feat.get_val('probe_coords')
    for p in pcs.keys():
        lst = pcs[p]
        newlst = map(lambda x:x-mn,lst)
        newpcs[p]=newlst
    if feat.strand()==-1:
        # reverse coords
        L = sum(map(lambda x:x.length(),feat.feats(type='exon')))
        tmp = mn
        mn = L - mx
        mx = L - tmp
    producer = feat.producer()
    newseq = feat.get_val('seq')[mn:(mx+probe_length)]
    gc_min = map_coord(feat,mn,type='exon')
    gc_max = map_coord(feat,mx,type='exon')
    newfeats = []
    subfeats = feat.feats(type='exon')
    subfeats.sort(lambda x,y:x.start()-y.start())
    for subfeat in subfeats:
        if (subfeat.start()+subfeat.length())<gc_min:
            continue
        if subfeat.start()>gc_max:
            break
        newfeat = Mapping.Feature.DNASeqFeature(display_id=subfeat.display_id(),
                                                producer=producer,
                                                start=subfeat.start(),
                                                length=subfeat.length(),
                                                strand=subfeat.strand(),
                                                type=subfeat.type(),
                                                seqname=feat.seqname())
        if newfeat.start()<gc_min:
            newstart = gc_min
            newlength = newfeat.start()+newfeat.length()-newstart
            newfeat.start(newstart)
            newfeat.length(newlength)
        if newfeat.start()+newfeat.length()>gc_max:
            newfeat.length(gc_max-newfeat.start())
        newfeats.append(newfeat)
    cfeat = ff.cfeat_from_feats(newfeats,display_id=feat.display_id(),
                                producer=producer,
                                group_feat_type='transcript',boundary_feat='exon')
    cfeat.set_key_val('seq',newseq)
    cfeat.set_key_val('probe_coords',newpcs)
    return cfeat

def trim_cons_feats(feats,probe_length=25):
    newfeats = []
    for feat in feats:
        try:
            new_feat = trim_cons_feat(feat,probe_length=probe_length)
            newfeats.append(new_feat)
        except IndexError:
            sys.stderr.write("Error: IndexError on: " + feat.display_id()+"\n")
    return newfeats

def add_probe_coords(fn='../annots/Mouse430_2_probe_tab.gz',
                     feats=None):
    """
    Function: Find the location of probe in each feat
              (consensus-to-genome alignment) 
    Returns :
    Args    :
    """
    i = 0
    fh = su.readfile(fn)
    fh.readline()
    map(lambda x:x.set_key_val('probe_coords',{}),feats)
    feats_d = ff.make_dict(feats)
    while 1:
        line = fh.readline()
        if not line:
            break
        toks = line.rstrip().split('\t')
        pseq = toks[4]
        ps = toks[0]
        # to uniquely identify a probe
        pxy = toks[1]+':'+toks[2]
        feat = None
        lst = None
        if feats_d.has_key(ps):
            lst = feats_d[ps]
        elif feats_d.has_key('A:'+ps):
            lst = feats_d['A:'+ps]
        elif feats_d.has_key('B:'+ps):
            lst = feats_d['B:'+ps]
        if lst and len(lst)>0:
            for feat in lst:
                cseq = feat.get_val('seq')
                if not cseq:
                    continue
                coords = get_probe_coords(cseq,pseq)
                if len(coords)>0:
                    d = feat.get_val('probe_coords')
                    d[pxy]=[]
                    for coord in coords:
                        d[pxy].append(coord)
                    i = i + 1
                    if i % 10000 == 0:
                        sys.stderr.write("Done: " + repr(i) + " probes." + \
                                         os.linesep)
    sys.stderr.write("Done!"+os.linesep)

def get_feats_list(groups):
    d ={}
    for group in groups:
        feats = group.get_feats()
        feats = filter(lambda x:x.producer()==chip_code,feats)
        for feat in feats:
            if d.has_key(feat.display_id()):
                sys.stderr.write("warning! two groups with same feat: "+\
                                 feat.display_id())
            else:
                d[feat.display_id()]=feat
    return d.values()
    
def add_fasta(fn='../annots/mm7.MOE430.fa.gz',
              feats=None,
              chip_code='MOE430',
              skip_uns=1,
              reporter=None):
    if not reporter:
        reporter = sys.stderr
    d = {}
    for feat in feats:
        did = feat.display_id().split(':')[-1]
        if not d.has_key(did):
            d[did]=[]
        d[did].append(feat)
    from Bio import SeqIO
    fh = su.readfile(fn)
    j = 0
    for seq_record in SeqIO.parse(fh,'fasta'):
        j = j + 1
        ps = seq_record.id.split(':')[-1]
        rng = seq_record.description.split()[1].split('=')[-1]
        seqname = rng.split(':')[0]
        if skip_uns and (seqname.startswith('chrUn')\
                         or seqname.endswith('random')):
            continue
        if not d.has_key(ps):
            continue
        sfeats = d[ps]
        for feat in sfeats:
            tag = feat.seqname()+':'+repr(feat.start()+1)+'-'+ \
                  repr(feat.start()+feat.length())
            if rng == tag:
                seq = seq_record.seq.data
                if not seq:
                    continue
                else:
                    feat.put('seq',seq)

def groups2link(groups,fn='mm7.Mouse430_2.link.psl',chip_code='MOE430',
                genome='Mouse_Aug_2005'):
    feats = []
    for group in groups:
        for feat in group.get_feats():
            if feat.producer() == chip_code:
                feats.append(feat)
    feats2link(feats,fn=fn,chip_code=chip_code,genome=genome)
    sys.stderr.write("Done writing: " + fn + os.linesep)

def feats2link(feats,fn='mm8.430_2.trimmed.link.psl',chip_code='MOE430',
               genome='M_Musculus_Mar_2006'):
    array = au.link_array_code(chip_code)
    fh = open(fn,'w')
    fh.write('# Filename: ' +  fn+os.linesep)
    fh.write('# Array: '+array+os.linesep)
    fh.write('# Genome: ' + genome + os.linesep)
    import time
    t = time.asctime()
    fh.write('# Date: ' + t + os.linesep)
    fh.write('#'+os.linesep)
    fh.write('# This PSL file contains alignments of Affymetrix consensus and exemplar'+os.linesep)
    fh.write('# sequences to a genome, and the mapping of Affymetrix probe sets' + os.linesep)
    fh.write('# onto those sequences.'+os.linesep+'# Feature types are segregated in blocks, each preceded by a track line.'+os.linesep+'#'+os.linesep)
    fh.write('track name="'+array+' netaffx consensus" description="Consensus Sequences"'+\
             os.linesep)
    for feat in feats:
        psl = cons_feat2psl(feat,chip_code=chip_code)
        fh.write(psl+os.linesep)
    fh.write('track name="'+array+' netaffx probesets " description="Probe Sets"'+os.linesep)
    for feat in feats:
        psl = cons_probes2psl(feat,chip_code=chip_code)
        fh.write(psl+os.linesep)
    fh.close()

"""
PSL lines represent alignments, and are typically taken from files generated by BLAT or psLayout. See the BLAT documentation for more details. All of the following fields are required on each data line within a PSL file:
	1 	matches - Number of bases that match that aren't repeats
	2 	misMatches - Number of bases that don't match
	3 	repMatches - Number of bases that match but are part of repeats
	4 	nCount - Number of 'N' bases
	5 	qNumInsert - Number of inserts in query
	6 	qBaseInsert - Number of bases inserted in query
	7 	tNumInsert - Number of inserts in target
	8 	tBaseInsert - Number of bases inserted in target
	9 	strand - '+' or '-' for query strand. For translated alignments, second '+'or '-' is for genomic strand
	10 	qName - Query sequence name
	11 	qSize - Query sequence size
	12 	qStart - Alignment start position in query
	13 	qEnd - Alignment end position in query
	14 	tName - Target sequence name
	15 	tSize - Target sequence size
	16 	tStart - Alignment start position in target
	17 	tEnd - Alignment end position in target
	18 	blockCount - Number of blocks in the alignment (a block contains no gaps)
	19 	blockSizes - Comma-separated list of sizes of each block
	20 	qStarts - Comma-separated list of starting positions of each block in query
	21 	tStarts - Comma-separated list of starting positions of each block in target (for minus strands, these should be end positions if the alignment is to the minus strand of the target
"""
def cons_feat2psl(feat,chip_code=None):
    exons = feat.sorted_feats(type='exon')
    qsize = sum(map(lambda x:x.length(),exons))
    f0 = '0'
    f1 = repr(qsize)
    f2 = '0'
    f3 = '0'
    f4 = '0'
    f5 = repr(len(exons)-1)
    f6 = repr(feat.length()-qsize)
    f7 = '0'
    f8 = '0'
    f9 = '+'
    if feat.strand()==-1:
        f9 = '-'
    f10 = au.link_array_code(chip_code) + ':' + feat.display_id().split(':')[-1]
    f11 = repr(qsize)
    f12 = '0'
    f13 = repr(qsize)
    f14 = feat.seqname()
    f15 = '0' # IGB doesn't care about the size of the target
    f16 = repr(feat.start())
    f17 = repr(feat.start()+feat.length())
    f18 = repr(len(exons))
    f19 = sep2.join(map(lambda x:repr(x.length()),exons))+sep2
    f20 = sep2.join(map(lambda x:repr(x),get_qstarts(feat)))+sep2
    f21 = sep2.join(map(lambda x:repr(x.start()),exons))+sep2
    line = sep1.join([f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f11,f12,
                      f13,f14,f15,f16,f17,f18,f19,f20,f21])
    return line

def get_qstarts(feat):
    exons = feat.sorted_feats(type='exon')
    #if feat.strand()==-1:
    # exons.reverse()
    qstarts = [0]
    i = 0
    for exon in exons[1:]:
        qstarts.append(exons[i].length()+qstarts[i])
        i = i + 1
    return qstarts

        
    
def cons_probes2psl(feat,chip_code=None,psize=25):
    exons = feat.feats(type='exon')
    tlen = sum(map(lambda x:x.length(),exons))
    probe_coords = []
    for lst in feat.get_val('probe_coords').values():
        probe_coords = probe_coords + lst
    probe_coords.sort(lambda x,y:su.num_cmp(x,y))
    nprobes = len(probe_coords)
    f1 = repr(nprobes * psize)
    f2 = '0'
    f3 = '0'
    f4 = '0'
    f5 = '0'
    f6 = '0'
    f7 = '0'
    f8 = '0'
    f9 = '+'
    f10 = 'P.'+au.link_array_code(chip_code)+':'+feat.display_id().split(':')[-1]
    f11 = repr(probe_coords[-1]-probe_coords[0]+psize)
    f12 = repr(probe_coords[0])
    f13 = repr(probe_coords[-1]+psize)
    f14 = au.link_array_code(chip_code)+':'+feat.display_id().split(':')[-1]
    f15 = repr(tlen)
    f16 = f12
    f17 = f13
    f18 = repr(nprobes)
    f19 = ''
    i = 0
    while i < nprobes:
        f19 = f19 + repr(psize)+sep2
        i = i + 1
    f20 = sep2.join(map(lambda x:repr(x),probe_coords))+sep2
    f21 = f20
    line = sep1.join([f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f11,f12,
                      f13,f14,f15,f16,f17,f18,f19,f20,f21])
    return line

"""
[Ann-Loraines-Computer:src/reanal/annots] annlorai% grep MOUSE430_2:1459680_AT Mouse430_2.link.psl 
0       11      154     2       0       0       2       2       -      MOUSE430_2:1459680_AT    437     261     428     chr16   0       1106636411066533        3       32,105,30,      9,41,146,       11066364,11066397,11066503,
145     292     0       0       0       0       0       0       +      P.MOUSE430_2:1459680_AT  437     90      268     MOUSE430_2:1459680_AT  437      90      268     11      25,25,25,25,25,25,25,25,25,25,25,      90,150,156,165,169,185,187,213,218,237,243,      90,150,156,165,169,185,187,213,218,237,243,
"""

def check_len(groups,chip_code='MOE430'):
    nomatches = []
    for g in groups:
        for feat in g.get_feats():
            if feat.producer() == chip_code:
                seqlen = len(feat.get_val('seq'))
                exons = feat.feats(type='exon')
                exon_len = sum(map(lambda x:x.length(),exons))
                if not seqlen == exon_len:
                    sys.stderr.write("Lengths don't match: " + \
                                     feat.display_id()+os.linesep)
                    nomatches.append(feat)
    return nomatches

def get_feat(groups,did):
    for group in groups:
        for feat in group.get_feats():
            if feat.display_id()==did:
                return feat
    

def make_bnib(v=None,seqname=None,
              seqfile=None,
              outfile=None,
              igb_home=None):
    """
    Function: Reads the given seqfile and converts it to an IGB
              bnib file
    Example : 
    Returns : 
    Args    : v - the genome version
              seqname - the name of the sequence
              seqfile - the fasta file containing the sequence
              outfile - the name of the 'bnib' file to write
                        (should end with extension .bnib)
              igb_home - the location of the 'genoviz' directory
                         which contains compiled IGB code
                         (run 'ant jar' to compile)
    """
    ihome = igb_home
    if not ihome:
        sys.stderr.write("Warning: igb_home not given. Attempting to get it from system environment variable IGB_HOME" + os.linesep)
        ihome = os.getenv('IGB_HOME')
        if not ihome:
            sys.stderr.write("Error: no environment variable IGB_HOME is set. Can't make bnibs with compiled IGB code."+os.linesep)
            return None
    s = su.readfile(seqfile)
    s.readline() # header
    seq = s.read() # the rest of the sequence
    subby = re.compile(r'\s') # to get rid of whitespaces
    seq = subby.sub('',seq)
    leng = len(seq)
    sys.stderr.write("size of: " + seqfile + " " + repr(leng) + " bp\n")
    cpath = ihome + os.sep + 'igb.jar' + ':' + ihome + os.sep + \
            'genoviz_sdk' + os.sep + 'genoviz.jar' + ':' + ihome + \
            os.sep + 'genometry' + os.sep + 'genometry.jar'
    sys.stderr.write("Using classpath: " + cpath + os.linesep)
    sys.stderr.write("======="+seqname+"======="+os.linesep)
    jcommand = "java -mx1024m -classpath " + cpath + " " +\
               "com.affymetrix.igb.parsers.NibbleResiduesParser " + \
               seqname + " " + seqfile + " " + outfile + " " + v
    sys.stderr.write("executing: " + jcommand+os.linesep)
    inf = os.popen(jcommand,'r')
    lines = inf.read()
    sys.stderr.write(lines)
    return leng


def zdummy():
    """
    Function:
    Returns :
    Args    :
    """
    pass

