# About

This repository contains (mostly) python code for managing the main IGB QuickLoad 
repository that provides sequence and annotation data files for the Integrated 
Genome Browser and related applications.

Most of the code here was developed to manage and create content for 
the main IGBQuickload site. 

To use:

* Clone the repository 
* Add the cloned repository root to your PYTHONPATH and PATH

To run tests, enter **Tests** directory and run **run_tests.py**.

# Main IGB Quickload site version control

The main IGB Quickload site is deployed for users at http://igbquickload.org/quickload.

The content for the main IGB Quickload site is version controlled using subversion, a 
centralized version control system. 

To browse the IGB Quickload subversion repository, visit https://svn.bioviz.org/viewvc.
The repository host uses [ViewVC](https://github.com/viewvc/viewvc) to display project files, 
commits, file history, etc.

# How to get IGB Quickload 

To replicate IGB Quickload on your computer, check it out using the svn command line client:

```
svn --username=guest --password=guest checkout https://svn.bioviz.org/repos/genomes/quickload 
```

The preceding command checks out the entire quickload repository into a the current working directory.
As of Oct, 2018, the quickload directory contains about 30 Gb of data.
Therefore it is generally more efficient to check out just a part of the repository. To do that, provide
the directory path you want. 

For example, check out the Homo sapiens (Dec 2013) GRCh38/hg38 genome assembly and annotations:

```
svn --username=guest --password=guest checkout https://svn.bioviz.org/repos/genomes/quickload/H_sapiens_Dec_2013
```

**Note**: The `guest` user is not able to commit (push) changes to the main repository. If you would like
to add a new genome, we recommend you set up the directory and files on your own server (e.g., an EC2
instance or similar) and then send us the URL for review. 

# Questions? 

Contact:

* Ann Loraine (aloraine@uncc.edu)

* * *

# License 

Copyright (c) Ann Loraine

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Also, see: http://opensource.org/licenses/MIT