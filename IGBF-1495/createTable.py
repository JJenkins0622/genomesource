#IGBF-1495 - Re-factor geneIdLookup.py to query dynamodb table instead of reading flat file
#createTable.py converts Araport11.bed file to Araport11.json file and creates table in dynamodb
from __future__ import print_function  # Python 2/3 compatibility
import boto3
import json
import csv

# Converts bed file to json file
data = []
with open('Araport11.bed', 'r') as f:
    reader = csv.reader(f, delimiter='\t')
    for itemGene in reader:
        toks = itemGene[3].split('.')
        Chromosome = itemGene[0]
        GeneId = toks[0]
        Start = int(itemGene[1])
        End = int(itemGene[2])
        print(GeneId)
        if not data:
            data.append({
                'Chromosome': Chromosome,
                'Start': Start,
                'End': End,
                'GeneId': GeneId,
            })
        #least Start and highest End
        else:
            for item in data:
                if GeneId == item['GeneId']:
                    flag = 1
                    if item['Start'] < Start:
                        Start = item['Start']
                    if item['End'] > End:
                        End = item['End']
                    item.update({
                        'Start': Start,
                        'End': End})
            if flag == 0:
                data.append({
                    'Chromosome': Chromosome,
                    'Start': Start,
                    'End': End,
                    'GeneId': GeneId
                    })
        flag = 0

#add "data" dictionary to json file
with open('Araport11.json', 'w') as outfile:
    json.dump(data, outfile)

#Creating table in dynamodb database
dynamodb = boto3.resource('dynamodb')
#Creation of table with partition key as GeneId
table = dynamodb.create_table(
    TableName='Araport11',
    KeySchema=[
        {
            'AttributeName': 'GeneId',
            'KeyType': 'HASH'  #Partition key
        }
    ],
    AttributeDefinitions=[
        {
            'AttributeName': 'GeneId',
            'AttributeType': 'S'
        }
    ],
    ProvisionedThroughput={
        'ReadCapacityUnits': 5,
        'WriteCapacityUnits': 5
    }
)

print("Araport11 Table status:", table.table_status)
