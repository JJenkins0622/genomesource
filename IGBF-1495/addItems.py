#IGBF-1495 - Re-factor geneIdLookup.py to query dynamodb table instead of reading flat file
# addItems.py adds json data into dynamodb
from __future__ import print_function  # Python 2/3 compatibility
import boto3
import json
import decimal
import csv
from boto3.dynamodb.conditions import Key, Attr
dynamodb = boto3.resource('dynamodb')
# table is an object for Araport11 dynamodb table
table = dynamodb.Table('Araport11')
# Copying data from Araport11.json to Araport11 table in dynamodb
with open("Araport11.json") as json_file:
    items = json.load(json_file, parse_float=decimal.Decimal)
    for item in items:
        Start = int(item['Start'])
        End = int(item['End'])
        Chromosome = item['Chromosome']
        GeneId = item['GeneId']

        print("Adding item:", GeneId)

        table.put_item(
           Item={
               'Start': Start,
               'End': End,
               'Chromosome': Chromosome,
               'GeneId': GeneId,
            }
        )