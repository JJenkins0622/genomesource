#!/usr/bin/env python

"""
Re-format gene_info.txt.gz to use probe sets as first column/primary key.

Invoke as:

  $ gunzip -c gene_info.txt.gz | gene_info2ps.py > ps_info.txt

"""
import fileinput,sys,gzip


def main():
    heads=False
    newheads=['ps','unique','redundant','AGI','symbol','descr','gene.type']
    sys.stdout.write('\t'.join(newheads)+'\n')
    for line in fileinput.input():
        toks=line.rstrip().split('\t')
        if not heads:
            # 'AGI\tsymbol\tdescr\tunique.ps\tpromiscuous.ps\tgene.type\n'
            heads=toks
            continue
        unique_pss=filter(lambda x:x!='NA',toks[3].split(';'))
        prom_pss=filter(lambda x:x!='NA',toks[4].split(';'))
        redundant='F'
        if len(unique_pss)>1:
            redundant='T'
        for ps in unique_pss:
            newtoks=[ps,'T',redundant,toks[0],toks[1],toks[2],toks[5]]
            sys.stdout.write('\t'.join(newtoks)+'\n')
        redundant='NA'
        for ps in prom_pss:
             newtoks=[ps,'F',redundant,toks[0],toks[1],toks[2],toks[5]]
             sys.stdout.write('\t'.join(newtoks)+'\n')

if __name__ == '__main__':
    main()
