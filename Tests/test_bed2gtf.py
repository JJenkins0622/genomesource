#!/usr/bin/env python

import unittest
import os,sys

import bed2gtf as b

class Bed2gtt_geneId(unittest.TestCase):
    
    bed6='data/Bed/100-lines.bed'
    bed14='data/Bed/20-linesBed14.bed'
    geneIdSet=( ('AT2G10650.1', 'AT2G10650'),
                ('AT1G36550.1', 'AT1G36550'),
                ('AT4G37080.2', 'AT4G37080'),
                ('AT5G42420.2', 'AT5G42420'),
                ('AT5G42420.10', 'AT5G42420'),
                ('AT5G42420.9', 'AT5G42420'),
                ('ATMG01310.1', 'ATMG01310'))    

    def setUp(self):
        # getFeats is taken from the script that is being tested
        self.feat6=b.getFeats(fname=self.bed6)
        self.feat14=b.getFeats(fname=self.bed14)

    def tearDown(self):
        del(self.feat6)
        del(self.feat14)
    
    def testGeneIdError(self):
        "getGeneId should fail if asked to take the gene id from column 13 of a bed 12 file."
        include_gene=13
        for i in range(1,20):
            self.assertRaises(ValueError,b.getGeneId,self.feat6[i],include_gene,'display_id')
    
    def testGeneIdValError(self):
        "getGeneId only accepts 4 and 13."
        include_gene=8
        for i in range(1,20):
            self.assertRaises(ValueError,b.getGeneId,self.feat6[i],include_gene,'display_id')
    
    def testGeneIdOnSplit(self):
        "given a bed14 file and -g4 , getGeneId should return the gene id from the transcript id."
        for display_id, right_answer in self.geneIdSet:
            include_gene=4
            result=b.getGeneId(self.feat6[4],include_gene,display_id)
            self.assertEquals(right_answer,result)
    
    def testGeneIdOnSplit(self):
        "given a bed14 file and -g 12, getGeneId should return the gene id from field 13."
        right_answer='ARV1'
        include_gene=13
        result=b.getGeneId(self.feat14[2],include_gene,'display.id')
        self.assertEquals(right_answer,result)

class Bed2gtf_notGeneId(unittest.TestCase):        

    bed6='data/Bed/100-lines.bed'
    bed14='data/Bed/20-linesBed14.bed'
    gtf_good="""chr1	NA	exon	3631	3913	.	+	.	transcript_id "AT1G01010.1";
chr1	NA	exon	3996	4276	.	+	.	transcript_id "AT1G01010.1";
chr1	NA	exon	4486	4605	.	+	.	transcript_id "AT1G01010.1";
chr1	NA	exon	4706	5095	.	+	.	transcript_id "AT1G01010.1";
chr1	NA	exon	5174	5326	.	+	.	transcript_id "AT1G01010.1";
chr1	NA	exon	5439	5899	.	+	.	transcript_id "AT1G01010.1";
chr1	NA	exon	5928	6263	.	-	.	transcript_id "AT1G01020.1";
chr1	NA	exon	6437	7069	.	-	.	transcript_id "AT1G01020.1";
chr1	NA	exon	7157	7232	.	-	.	transcript_id "AT1G01020.1";
chr1	NA	exon	7384	7450	.	-	.	transcript_id "AT1G01020.1";
chr1	NA	exon	7564	7649	.	-	.	transcript_id "AT1G01020.1";
chr1	NA	exon	7762	7835	.	-	.	transcript_id "AT1G01020.1";
chr1	NA	exon	7942	7987	.	-	.	transcript_id "AT1G01020.1";
chr1	NA	exon	8236	8325	.	-	.	transcript_id "AT1G01020.1";
chr1	NA	exon	8417	8464	.	-	.	transcript_id "AT1G01020.1";
chr1	NA	exon	8571	8737	.	-	.	transcript_id "AT1G01020.1";
chr1	NA	exon	6790	7069	.	-	.	transcript_id "AT1G01020.2";
chr1	NA	exon	7157	7450	.	-	.	transcript_id "AT1G01020.2";
chr1	NA	exon	7564	7649	.	-	.	transcript_id "AT1G01020.2";
chr1	NA	exon	7762	7835	.	-	.	transcript_id "AT1G01020.2";
chr1	NA	exon	7942	7987	.	-	.	transcript_id "AT1G01020.2";
chr1	NA	exon	8236	8325	.	-	.	transcript_id "AT1G01020.2";
chr1	NA	exon	8417	8464	.	-	.	transcript_id "AT1G01020.2";
chr1	NA	exon	8571	8737	.	-	.	transcript_id "AT1G01020.2";
chr1	NA	exon	11649	13173	.	-	.	transcript_id "AT1G01030.1";
chr1	NA	exon	13335	13714	.	-	.	transcript_id "AT1G01030.1";
chr1	NA	exon	23146	24451	.	+	.	transcript_id "AT1G01040.1";
chr1	NA	exon	24542	24655	.	+	.	transcript_id "AT1G01040.1";
chr1	NA	exon	24752	24962	.	+	.	transcript_id "AT1G01040.1";
chr1	NA	exon	25041	25435	.	+	.	transcript_id "AT1G01040.1";
chr1	NA	exon	25524	25743	.	+	.	transcript_id "AT1G01040.1";
chr1	NA	exon	25825	25997	.	+	.	transcript_id "AT1G01040.1";
chr1	NA	exon	26081	26203	.	+	.	transcript_id "AT1G01040.1";
chr1	NA	exon	26292	26452	.	+	.	transcript_id "AT1G01040.1";
chr1	NA	exon	26543	26776	.	+	.	transcript_id "AT1G01040.1";
chr1	NA	exon	26862	27012	.	+	.	transcript_id "AT1G01040.1";
chr1	NA	exon	27099	27281	.	+	.	transcript_id "AT1G01040.1";
chr1	NA	exon	27372	27533	.	+	.	transcript_id "AT1G01040.1";
chr1	NA	exon	27618	27713	.	+	.	transcript_id "AT1G01040.1";
chr1	NA	exon	27803	28431	.	+	.	transcript_id "AT1G01040.1";
chr1	NA	exon	28708	28805	.	+	.	transcript_id "AT1G01040.1";
chr1	NA	exon	28890	29080	.	+	.	transcript_id "AT1G01040.1";
chr1	NA	exon	29160	30065	.	+	.	transcript_id "AT1G01040.1";
chr1	NA	exon	30147	30311	.	+	.	transcript_id "AT1G01040.1";
chr1	NA	exon	30410	30816	.	+	.	transcript_id "AT1G01040.1";
chr1	NA	exon	30902	31227	.	+	.	transcript_id "AT1G01040.1";
chr1	NA	exon	23416	24451	.	+	.	transcript_id "AT1G01040.2";
chr1	NA	exon	24542	24655	.	+	.	transcript_id "AT1G01040.2";
chr1	NA	exon	24752	24962	.	+	.	transcript_id "AT1G01040.2";
chr1	NA	exon	25041	25435	.	+	.	transcript_id "AT1G01040.2";
chr1	NA	exon	25524	25743	.	+	.	transcript_id "AT1G01040.2";
chr1	NA	exon	25825	25997	.	+	.	transcript_id "AT1G01040.2";
chr1	NA	exon	26081	26203	.	+	.	transcript_id "AT1G01040.2";
chr1	NA	exon	26292	26452	.	+	.	transcript_id "AT1G01040.2";
chr1	NA	exon	26543	26776	.	+	.	transcript_id "AT1G01040.2";
chr1	NA	exon	26862	27012	.	+	.	transcript_id "AT1G01040.2";
chr1	NA	exon	27099	27281	.	+	.	transcript_id "AT1G01040.2";
chr1	NA	exon	27372	27536	.	+	.	transcript_id "AT1G01040.2";
chr1	NA	exon	27618	27713	.	+	.	transcript_id "AT1G01040.2";
chr1	NA	exon	27803	28431	.	+	.	transcript_id "AT1G01040.2";
chr1	NA	exon	28708	28805	.	+	.	transcript_id "AT1G01040.2";
chr1	NA	exon	28890	29080	.	+	.	transcript_id "AT1G01040.2";
chr1	NA	exon	29160	30065	.	+	.	transcript_id "AT1G01040.2";
chr1	NA	exon	30147	30311	.	+	.	transcript_id "AT1G01040.2";
chr1	NA	exon	30410	30816	.	+	.	transcript_id "AT1G01040.2";
chr1	NA	exon	30902	31120	.	+	.	transcript_id "AT1G01040.2";
chr1	NA	exon	28500	28706	.	+	.	transcript_id "AT1G01046.1";
chr1	NA	exon	31170	31424	.	-	.	transcript_id "AT1G01050.1";
chr1	NA	exon	31521	31602	.	-	.	transcript_id "AT1G01050.1";
chr1	NA	exon	31693	31813	.	-	.	transcript_id "AT1G01050.1";
chr1	NA	exon	31933	31998	.	-	.	transcript_id "AT1G01050.1";
chr1	NA	exon	32088	32195	.	-	.	transcript_id "AT1G01050.1";
chr1	NA	exon	32282	32347	.	-	.	transcript_id "AT1G01050.1";
chr1	NA	exon	32431	32459	.	-	.	transcript_id "AT1G01050.1";
chr1	NA	exon	32547	32670	.	-	.	transcript_id "AT1G01050.1";
chr1	NA	exon	33029	33153	.	-	.	transcript_id "AT1G01050.1";
chr1	NA	exon	33379	33589	.	-	.	transcript_id "AT1G01060.3";
chr1	NA	exon	33981	34327	.	-	.	transcript_id "AT1G01060.3";
chr1	NA	exon	34401	35474	.	-	.	transcript_id "AT1G01060.3";
chr1	NA	exon	35567	35647	.	-	.	transcript_id "AT1G01060.3";
chr1	NA	exon	35730	35963	.	-	.	transcript_id "AT1G01060.3";
chr1	NA	exon	36624	36685	.	-	.	transcript_id "AT1G01060.3";
chr1	NA	exon	36810	36921	.	-	.	transcript_id "AT1G01060.3";
chr1	NA	exon	37023	37203	.	-	.	transcript_id "AT1G01060.3";
chr1	NA	exon	37373	37398	.	-	.	transcript_id "AT1G01060.3";
chr1	NA	exon	37569	37757	.	-	.	transcript_id "AT1G01060.3";
chr1	NA	exon	33666	34327	.	-	.	transcript_id "AT1G01060.2";
chr1	NA	exon	34401	35474	.	-	.	transcript_id "AT1G01060.2";
chr1	NA	exon	35567	35647	.	-	.	transcript_id "AT1G01060.2";
chr1	NA	exon	35730	35963	.	-	.	transcript_id "AT1G01060.2";
chr1	NA	exon	36624	36685	.	-	.	transcript_id "AT1G01060.2";
chr1	NA	exon	36810	36921	.	-	.	transcript_id "AT1G01060.2";
chr1	NA	exon	37023	37203	.	-	.	transcript_id "AT1G01060.2";
chr1	NA	exon	37373	37780	.	-	.	transcript_id "AT1G01060.2";
chr1	NA	exon	33666	34327	.	-	.	transcript_id "AT1G01060.4";
chr1	NA	exon	34401	35471	.	-	.	transcript_id "AT1G01060.4";
chr1	NA	exon	35567	35647	.	-	.	transcript_id "AT1G01060.4";
chr1	NA	exon	35730	35963	.	-	.	transcript_id "AT1G01060.4";
chr1	NA	exon	36624	36685	.	-	.	transcript_id "AT1G01060.4";
chr1	NA	exon	36810	36921	.	-	.	transcript_id "AT1G01060.4";
chr1	NA	exon	37023	37203	.	-	.	transcript_id "AT1G01060.4";
chr1	NA	exon	37373	37398	.	-	.	transcript_id "AT1G01060.4";
chr1	NA	exon	37569	37780	.	-	.	transcript_id "AT1G01060.4";
chr1	NA	exon	33666	34327	.	-	.	transcript_id "AT1G01060.1";
chr1	NA	exon	34401	35474	.	-	.	transcript_id "AT1G01060.1";
chr1	NA	exon	35567	35647	.	-	.	transcript_id "AT1G01060.1";
chr1	NA	exon	35730	35963	.	-	.	transcript_id "AT1G01060.1";
chr1	NA	exon	36624	36685	.	-	.	transcript_id "AT1G01060.1";
chr1	NA	exon	36810	36921	.	-	.	transcript_id "AT1G01060.1";
chr1	NA	exon	37023	37203	.	-	.	transcript_id "AT1G01060.1";
chr1	NA	exon	37373	37398	.	-	.	transcript_id "AT1G01060.1";
chr1	NA	exon	37569	37840	.	-	.	transcript_id "AT1G01060.1";
chr1	NA	exon	33967	34327	.	-	.	transcript_id "AT1G01060.5";
chr1	NA	exon	34401	35474	.	-	.	transcript_id "AT1G01060.5";
chr1	NA	exon	35567	35647	.	-	.	transcript_id "AT1G01060.5";
chr1	NA	exon	35730	35999	.	-	.	transcript_id "AT1G01060.5";
chr1	NA	exon	36090	36171	.	-	.	transcript_id "AT1G01060.5";
chr1	NA	exon	36624	36685	.	-	.	transcript_id "AT1G01060.5";
chr1	NA	exon	36810	36921	.	-	.	transcript_id "AT1G01060.5";
chr1	NA	exon	37023	37203	.	-	.	transcript_id "AT1G01060.5";
chr1	NA	exon	37373	37871	.	-	.	transcript_id "AT1G01060.5";
chr1	NA	exon	38752	39054	.	-	.	transcript_id "AT1G01070.2";
chr1	NA	exon	39136	39287	.	-	.	transcript_id "AT1G01070.2";
chr1	NA	exon	39409	39814	.	-	.	transcript_id "AT1G01070.2";
chr1	NA	exon	40213	40329	.	-	.	transcript_id "AT1G01070.2";
chr1	NA	exon	40473	40927	.	-	.	transcript_id "AT1G01070.2";
chr1	NA	exon	38752	39054	.	-	.	transcript_id "AT1G01070.1";
chr1	NA	exon	39136	39287	.	-	.	transcript_id "AT1G01070.1";
chr1	NA	exon	39409	39814	.	-	.	transcript_id "AT1G01070.1";
chr1	NA	exon	40213	40329	.	-	.	transcript_id "AT1G01070.1";
chr1	NA	exon	40473	40535	.	-	.	transcript_id "AT1G01070.1";
chr1	NA	exon	40675	40944	.	-	.	transcript_id "AT1G01070.1";
chr1	NA	exon	44677	44787	.	+	.	transcript_id "AT1G01073.1";
chr1	NA	exon	45296	45559	.	-	.	transcript_id "AT1G01080.1";
chr1	NA	exon	45646	45954	.	-	.	transcript_id "AT1G01080.1";
chr1	NA	exon	46044	46145	.	-	.	transcript_id "AT1G01080.1";
chr1	NA	exon	46376	47019	.	-	.	transcript_id "AT1G01080.1";
chr1	NA	exon	45296	45559	.	-	.	transcript_id "AT1G01080.2";
chr1	NA	exon	45646	45954	.	-	.	transcript_id "AT1G01080.2";
chr1	NA	exon	46044	46145	.	-	.	transcript_id "AT1G01080.2";
chr1	NA	exon	46373	47019	.	-	.	transcript_id "AT1G01080.2";
chr1	NA	exon	47485	47982	.	-	.	transcript_id "AT1G01090.1";
chr1	NA	exon	48075	48852	.	-	.	transcript_id "AT1G01090.1";
chr1	NA	exon	48936	49286	.	-	.	transcript_id "AT1G01090.1";
chr1	NA	exon	50075	50337	.	-	.	transcript_id "AT1G01100.2";
chr1	NA	exon	50419	50631	.	-	.	transcript_id "AT1G01100.2";
chr1	NA	exon	50883	50963	.	-	.	transcript_id "AT1G01100.2";
chr1	NA	exon	51126	51199	.	-	.	transcript_id "AT1G01100.2";
"""
    
    def testGetExtraFeat(self):
        "given an transcript id and a gene id, getExtraFeat should puth them together into a string."
        display_id='AT1G36630.1'
        gene_id='AT1G36630'
        include_gene=13
        right_answer='transcript_id "AT1G36630.1"; gene_id "AT1G36630";'
        result=b.getExtraFeat(display_id, gene_id, include_gene)
        self.assertEquals(right_answer,result)

    def testNoGeneIdOption(self):
        "the lack of the include_gene option should not create a problem."
        b.main(self.bed6)
        b.main(self.bed14)

# result does not capture the output of main, which is written to standard out.
#    def testGetExtraFeat(self):
#        "The ouptut of main on the sample file bed14 should look like the gtf shown above."
#        result=b.main(self.bed14)
#        self.assertEquals(self.gtf_good,result)

if __name__ == "__main__":
    unittest.main()


    
