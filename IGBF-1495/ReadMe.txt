1. CreateTable.py : It converts Araport11.bed to Araport11.json file. It also creates Araport11 table in dynamoDb

2. addItems.py : It adds the json data to the dynamodb
	-->Adding is done row by row, writing speed depends on the Write Capacity cofiguration while creating the 		table.
	-->Current Write Capacity configuraton is set to 20. It took 18mins approx to transfer data from ec2 instance 	to dynamoDb
	-->Direct upload is not possible using SDK. However there is a possibility to upload the file to dynamodb 	with s3 bucket. 

3. geneIdLookup.py: Loads gene data from dynamoDB instead of flatFile.

6. There are two ways to access the crentials.
	a. From some config file
	b. Assign the role to the instance and read the keys from the metadata. (currently used method)	

