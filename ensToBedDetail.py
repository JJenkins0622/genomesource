#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  1 16:54:51 2019
@author: jilljenkins

This script will accept a UCSC Table Browser ensGene BED file and merge it with
an Ensembl Biomart file generating a BED14/detail file. To receive the desired
output below, the Biomart file must be in the specific user-defined format
described below.

To create the Biomart file:

* Go to Ensembl Biomart installation, for example:
    http://useast.ensembl.org/biomart/martview/22277bc12da5624bbf2728be0832e026
* Select database (e.g., Ensembl Genes 95)
* Choose Dataset = specific to the organism
* Select options below

Filters:
    Your first time on the site, it will default to some selections,
    unselect all filters. The filter parameter should read [None selected]
    to move on to adding Attributes.

Attributes:
    For this script to work, the following Attributes must be selected in the
    specified order, and will be found under +GENE:

        Transript stable ID version
        Gene stable ID version
        Gene name
        Gene description

Results(top of left margin options)
    Ensure 'Export all results to' has been selected as 'File' and 'TSV'
    Hit 'Go'

A file with name mart_export.tsv has been added to your downloads directory.

Obtain an ensGene database file from UCSC table browser and run this
script.
"""
import optparse, sys

def readMartFile(fname=None):
    file1=open(fname)
    martDict = {}
    for line in file1:
        fields = line.strip().split('\t')
        if len(fields) == 3:
            fields.append('NA')
            field14 = ' '.join(fields[2:])
            martDict[fields[0]] = fields[1] + '\t' + field14
        elif len(fields) == 2:
            N = ['NA','NA']
            fields.extend(N)
            field14 = '\t'.join(fields[2:])
            martDict[fields[0]] = fields[1] + '\t' + field14
        else:
            field14 = ' '.join(fields[2:])
            martDict[fields[0]] = fields[1] + '\t' + field14
    return martDict

def main(bed_file=None,biomart_file=None,out_file=None):
    martDict = readMartFile(biomart_file)
    if out_file:
        outfile = open(out_file,'w')
    else:
        outfile = sys.stdout
    #read in second file use field 3 (Ensembl gene ID) to search dictionary
    file2 = open(bed_file)
    for line in file2:
        fields = line.strip('\n').split('\t')
        # add the new fields to the BED12 file
        if fields[3] in martDict:
            line = '\t'.join(fields) + '\t' + martDict[fields[3]]
            outfile.write(line + '\n')
    if out_file:
        outfile.close()

if __name__ == '__main__':
    usage = "%prog [options]"
    parser = optparse.OptionParser(usage)
    parser.add_option("-m","--biomart_file",help="GFF3 file to convert. Can be compressed. [required]",dest="biomart_file"),
    parser.add_option("-b","--bed_file",help="BED12 file to read (from UCSC) [required]",
                      dest="bed_file",default=None)
    parser.add_option("-o","--out_file",help="BED14 file to write [optional]",dest="out_file")
    (options,args)=parser.parse_args()
    if not options.bed_file or not options.biomart_file:
        parser.error("Bed or GFF files not specified.")
    if not options.out_file:
        out_file = None
    else:
        out_file = options.out_file
    main(biomart_file=options.biomart_file,
         bed_file=options.bed_file,
         out_file=out_file)
