"""Utility methods for working with features, especially cleaning, screening, and counting."""

import sys,os
import Mapping.FeatureModel as feature

def calc_K(feats):
    """
    Function: Calculate the number of bases covered by the given list of
              CompoundDNASeqFeature objects. Expects regions containing
              bases have feat_type "exon"
    Returns : number of bases covered by the list of CompoundDNASeqFeatures
    Args    : feats - a list of Mapping.Feature.CompoundDNASeqFeatures
    """
    feat_vector = calc_feat_vector(feats)
    return (len(filter(lambda x:x>0,feat_vector)))/1000.

def calc_feat_vector(feats):
    """
    Function: Build an array where each position in the array represents
              a base covered by one or more feats. Values in the array represent
              the number of different feats that cover the base.
    Returns : array with isoform counts per base (a depth graph)
    Args    : feats - a list of Mapping.Feature.CompoundDNASeqFeatures
    """
    min_start = min(map(lambda x:x.getStart(),feats))
    max_end = max(map(lambda x:x.getStart()+x.getLength(),feats))
    gn = zerros(max_end-min_start)
    N = len(gn)
    for feat in feats:
        exons = feat.getFeats(feat_type='exon')
        for exon in exons:
            for base in range(exon.getStart()-min_start,exon.getStart()+exon.getLength()-min_start):
                gn[base]=gn[base]+1
    return gn

def zerros(val):
    z = range(0,val)
    for i in z:
        z[i]=0
    return z

def check_exon_bounds(feat):
    """
    Function: Check that the start of each subfeature is >= the end position
              of the feature immediately preceding it.
    Returns : nothing, if no features violate the rule
              the feature, if the rule is violated
    Args    : feat - a Mapping.Feature.CompoundSeqFeature
              type - the type of sub-feature to check (e.g., exon)
    """
    sorted_feats = feat.sorted_feats(type='exon')
    i = 0
    for sub_feat in sorted_feats[:-1]:
        i = i + 1
        next_feat = sorted_feats[i]
        if sub_feat.start() + sub_feat.length() > next_feat.start():
            return next_feat
    return None

def remove_uns(feats):
    """
    Function: Remove features that map onto sequences with
              'random' or 'Un' in the name
    Returns : a list of features
    Args    : a list of features
    """
    cleaned_up = filter(lambda x:not x.seqname().endswith('random') and \
                        not x.seqname().startswith('chrUn'),feats)
    return cleaned_up

def cleanup_feats(feats):
    """
    Function: Remove multi-mapping, non-protein-coding, 'random' or ChrUn-
              mapping features
    Returns : a list of features
    Args    : a list of features

    Multi-mapping means: features that map to multiple locations in
    a genome, e.g., to multiple chromosome assemblies.
    """
    n = remove_uns(feats)
    n = remove_noncoding(n)
    n = remove_multi_mapping(n)
    return n

def remove_noncoding(feats):
    """
    Function: Remove features that lack ORF sub-features
    Returns : a list of features
    Args    : a list of features
    """
    return filter(lambda x:x.feats(type='ORF'),feats)
    
def remove_multi_mappers(feats):
    """
    Function: Remove features that map to multiple locations
              (Remove features that have the same display_id)
    Returns : a list of features
    Args    : a list of features
    """
    d = {}
    for feat in feats:
        id = feat.getDisplayId()
        if not d.has_key(id):
            d[id]=[]
        d[id].append(feat)
    l = []
    for id in d.keys():
        if len(d[id])==1:
            l.append(d[id][0])
    return l

def make_dict(feats,strip_namespace=0):
    """
    Function: Create a dictionary containing the given feature
              objects
    Returns : a dictionary, keys are feature display_id's, and values
              are lists of features with those display_id's
    Args    : feats - a list of features (or anything with a display_id)
    """
    d = {}
    for feat in feats:
        key = feat.displayId()
        if strip_namespace:
            key = feat.getDisplayId().split(':')[-1]
        if d.has_key(key):
            d[key].append(feat)
        else:
            d[key]=[feat]
    return d


def write_dids(feats,fn=None):
    """
    Function: Write a list of given feats' display ids to
              a file called fn, or sys.stderr if None
    Returns :
    Args    : feats - a list of objects with display_id's
              fn - the name of a file to write
    """
    if not fn:
        fh = sys.stderr
    else:
        fh = open(fn,'w')
    for feat in feats:
        fh.write(feat.getDisplayId()+os.linesep)
    if fn:
        fh.close()


def writeLocusToTxLength(fname='tx_size.txt',d=None,sep='\t'):
    """
    Function: Write a file with transcript sizes
    Returns :
    Args    : feats - dictionary with locus ids as keys, transcript
                      sizes as values
              fname - the name of a file to write [optional]

    If fname not given, writes to stdout.
    """
    if fname:
        fh = open(fname,'w')
    else:
        fh = sys.stdout
    toks=['locus','bp']
    fh.write(sep.join(toks)+'\n')
    for locus_id in d.keys():
        toks=[locus_id,str(d[locus_id])]
        fh.write(sep.join(toks)+'\n')
    if fname:
        fh.close()

