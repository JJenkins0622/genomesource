"""Functions for reading Phytozome GFF3 files."""

import Mapping.FeatureModel as feature
import Utils.General as utils
import Mapping.FeatureFactory as ff
import sys

"""
This contains functions for parsing thellungiella.org's variant of
GFF, which defines gene models using features of the following types:

CDS
five_prime_UTR
mRNA
polyA_signal_sequence
polyA_site
start_codon
stop_codon
three_prime_UTR
transcription_start_site

For parsing, we only use features of type mRNA, five_prime_UTR, three_prime_UTR, CDS.

The file contains a header row that identifies the fields.

The extra feature field in mRNA lines contains two values: a gene
model identifier (e.g., Tp100010) and a Note. The boundaries of the mRNA feature
seem wrong; they don't coincide with the start of the five_prime_UTR or the end
of the three_prime_UTR features. For now, we'll ignore the mRNA start and end but
use the name and meta-data information.

The gene model identifier is used to group features belonging to the same gene model,
which represents a spliced transcript.

The Note value will be added to the key/value dictionary attached to the mRNA
feature, using key "Note."

If converted to BED14 format, the detail (BED14), using the gene model
identifier in fields four and thirteen and the Note attribute for the
fourteenth field. In the case of the parvula genome annotations current as of
Dec 2012, the Notes typically comment on similiarity with homologous genes from
Arabidosis thaliana.

How this works:

Read the GFF and create exon features from CDS, three_prime_UTR and five_prime_UTR.
Also create CDS features from CDS lines. Group these features into a CompoundDNASeqFeature
object of feature type mRNA. Merge the exons using FeatureFactory mergeExons method.
CDS features will be used to delimite the start and stop of translation, i.e., the
thickStart and thickStop from BED format. The CDS includes the stop codon.

See:

http://thellungiella.org/
http://thellungiella.org/data/
"""
def gff2feats(fname=None):
    """
    Function: read features from T. parvula GFF format file
    Returns : list of CompoundDNASeqFeature objects representing spliced
              mRNA transcripts
    Args    : fname - name of file to read
    """
    lineNumber = 0
    fh = utils.readfile(fname)
    mRNAs = {}
    features={}
    # first get the mRNAs
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        if line.startswith('#') or line.startswith('seqname'):
            lineNumber = lineNumber + 1
            continue
        vals = line.rstrip().split('\t')
        try:
            key_vals = parseKeyVals(vals[-1])
        except IndexError:
            sys.stderr.write("Problem with line %i: %s"%(lineNumber,line))
            raise
        ID=key_vals['ID']
        feat_type=vals[2]
        if feat_type == 'mRNA':
            if mRNAs.has_key(ID):
                raise ValueError("Two mRNAs have the same ID: %s"%ID)
            feat = ff.makeFeatFromGffFields(vals=vals[0:-1],
                                                       cfeat=True)
            feat.setDisplayId(ID)
            feat.setKeyVals(key_vals)
            mRNAs[ID]=feat
        else:
            # ignore everything but these 
            if feat_type in ['CDS','three_prime_UTR','five_prime_UTR']:
                feat = ff.makeFeatFromGffFields(vals=vals[0:-1],
                                                cfeat=False)
                feat.setFeatType('exon')
                if features.has_key(ID):
                    features[ID].append(feat)
                else:
                    features[ID]=[feat]
                if feat_type == 'CDS':
                    # make a new one; we use CDSs to delimit start and stop of translation
                    feat = ff.makeFeatFromGffFields(vals=vals[0:-1],cfeat=False)
                    features[ID].append(feat)
        lineNumber = lineNumber + 1
    for ID in mRNAs.keys():
        mRNA=mRNAs[ID]
        subfeatures=features[ID]
        for subfeature in subfeatures:
            mRNA.addFeat(subfeature)
        ff.mergeExons(mRNA)
        # now fix mRNA coordinates
        # the GFF file with gene models TpV84_ORFs.gff
        # has too small start and too big end for mRNA features
        exons = mRNA.getSortedFeats('exon')
        new_start = exons[0].getStart()
        try:
            new_length = exons[-1].getEnd() - new_start
        except TypeError:
            return mRNA
        mRNA.setStart(new_start)
        mRNA.setLength(new_length)
    return mRNAs.values()

def parseKeyVals(extra_feat):
    "Parse extra feature fields."
    vals = extra_feat.split(';')
    name=vals[0]
    key_vals = {'ID':name,'gene_name':name}
    if len(vals)>1:
        note=vals[1]
        if note.startswith('Note'):
            # looks like 'Note "similar to AT3G52090|NRPB11"'
            note = note[6:-1] # getting ride of Note prefix and quote marks
            key_vals['Note']=note
        else:
            raise ValueError("Unexpected format for Note in: %s"%extra_feat)
    return key_vals
            
        
